import { HeroSection } from '../components/HeroSection';
import { About } from '../components/About';
import { Client } from '../components/Client';
import { Trainers } from '../components/Trainers';
import { TimeSchedule } from '../components/TimeSchedule';
import { Contact } from '../components/Contact';
import { Blog } from '../components/Blog';
import { Services } from '../components/Services';
import { Pricing } from '../components/Pricing';
import { Gyming } from '../components/Gyming'


export const Home = () => {
    return (
        <div className='App'>

            <HeroSection />
            <About />
            <Services />
            <Gyming />
            <TimeSchedule />

            <Trainers />
            {/* <Services /> */}

            <Client />
            {/* <Gallery /> */}
            <Pricing />


            <Contact />
            <Blog />


        </div>
    )
}
