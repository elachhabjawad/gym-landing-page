import { useState } from 'react';
import { Outlet } from 'react-router-dom';
import { Nav } from './Nav';
import { Footer } from './Footer';

export const Layout = () => {



    
    const [nav, setNav] = useState(false);

    window.addEventListener("scroll", () => {
        const scroll = document.documentElement.scrollTop;
        if (scroll > 405) {
            setNav(true);
        }
        else {
            setNav(false);
        }
    })



    return (
        <>
            <Nav nav={nav} />
            <main>
                <Outlet />
            </main>
            <Footer nav={nav} />
        </>
    )
}
