import { AiOutlineArrowRight } from "react-icons/ai";
import image1 from '../assets/services/img-1.jpg';
import image2 from '../assets/services/img-2.jpg';
import image3 from '../assets/services/img-3.jpg';
import image4 from '../assets/services/img-4.jpg';

export const Services = () => {


    const servicesData = [
        {
            title: "GRIT STRENGTH",
            content: "We have heap of fun piece of equi to build down every inh of your for body.",
            image: image1,
        },
        {
            title: "ZUMBA ATHLETIC",
            content: "We have heap of fun piece of equi to build down every inh of your for body.",
            image: image2,
        },
        {
            title: "FUSION YOGA",
            content: "We have heap of fun piece of equi to build down every inh of your for body.",
            image: image3,
        },

        {
            title: "MEDITATION",
            content: "We have heap of fun piece of equi to build down every inh of your for body.",
            image: image4,
        }
    ];


    return (
        <section id="services" className='w-full grid grid-cols-4  max-lg:grid-cols-2 max-lg:grid-rows-4 max-lg:gap-4 max-sm:grid-cols-1 max-sm:grid-rows-5 max-sm:gap-x-0'>

            {servicesData.map(({ title, content, image }, index) =>

                <div key={index} style={{ backgroundImage: `url(${image})` }} className='group h-[65vh]  relative overflow-hidden  after:absolute after:top-0 after:left-[-100%] hover:after:left-0 after:w-full after:h-full after:bg-gradient-to-r from-black bg-cover max-sm:row-span-1 max-sm:col-span-1 '>
                    <div className='w-full group-hover:left-[50%] duration-300 absolute top-[50%] left-[-50%] translate-x-[-50%] translate-y-[-50%] text-center z-10'>
                        <div className=' bg-[#f04e3c] rounded-full p-4 max-lg:p-2 text-white text-2xl mb-7 max-lg:mb-3 w-fit m-auto cursor-pointer hover:bg-white hover:text-[#f04e3c] duration-300'>
                            <AiOutlineArrowRight />
                        </div>
                        <h1 className=' text-4xl text-white font-medium mb-2'>{title}</h1>
                        <p className=' font text-xl text-slate-200'>{content}</p>
                    </div>
                </div>

            )}

        </section>
    )
}
