
export const Pricing = () => {



    const pricingData = [
        {
            title: "BASIC PLAN",
            price: "49",
            features: [
                "Get Free WiFi",
                "Month to Month",
                "No Time Restrictions",
                "Gym and Cardio",
                "Service Locker Rooms",
            ],
        },
        {
            title: "ADVANCE",
            price: "69",
            features: [
                "Get Free WiFi",
                "Month to Month",
                "No Time Restrictions",
                "Gym and Cardio",
                "Service Locker Rooms",
            ],
        },
        {
            title: "PREMIUM",
            price: "99",
            features: [
                "Get Free WiFi",
                "Month to Month",
                "No Time Restrictions",
                "Gym and Cardio",
                "Service Locker Rooms",
            ],
        },
    ];




    return (
        <section id='pricing' className=' w-full min-h-screen bg-pricing-pattern bg-cover bg-fixed max-lg:bg-center max-sm:bg-center'>
            <div className=' max-container padding-hero-y padding-x'>
                <p className=' text-[#f04e3c] relative before:absolute before:w-20 before:h-1 before:bg-[#f04e3c] before:top-[50%] before:left-0 pl-24 text-2xl before:translate-y-[-50%] text-center w-fit m-auto max-sm:before:w-16 max-sm:pl-20'>INCLUSIVE, HARASSMENT-FREE EVENTS</p>

                <div className='text-white max-w-[40%] m-auto text-6xl font-semibold leading-[70px] mt-5 mb-20 text-center max-lg:text-5xl max-lg:leading-[50px] max-md:max-md:max-w-[100%] max-sm:text-3xl'>
                    <h1>PRICING PLAN</h1>
                </div>

                <div className="w-full grid grid-cols-3 gap-10 max-lg:grid-cols-2 max-lg:grid-rows-4 max-lg:gap-4 max-sm:grid-cols-1 max-sm:grid-rows-5 max-sm:gap-x-0">


                    {pricingData.map(({ title, price, features }, index) =>
                        <div key={index} className="bg-white p-7">
                            <div className="text-center">
                                <div className="text-lg font-semibold">
                                    <p className=' text-2xl text-center w-fit m-auto max-sm:before:w-16'>{title}</p>

                                </div>

                                <div className="py-8">
                                    <span className="text-[#f04e3c] text-7xl font-medium">${price}</span>
                                    <span className="font text-md text-slate-500">/Mo</span>
                                </div>

                                <ul className="space-y-4 ">
                                    {features.map((feature, index) =>
                                        <li key={index} className=" font text-md text-slate-500">{feature}</li>
                                    )}
                                </ul>

                                <button className='mt-10 py-4 px-7 text-xl group relative text-white bg-[orangered]' >
                                    <div className='buttonDiv'></div>
                                    <span className='buttonSpan'>Enroll Now </span>
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </section>
    )
};