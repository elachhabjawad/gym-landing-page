import { motion } from 'framer-motion';

export const BannerPage = ({ title }) => {
    return (
        <section id='home' className='w-screen h-[450px] bg-hero-pattern bg-cover bg-fixed bg-left max-sm:bg-center max-lg:bg-center'>
            <motion.div
                initial={{ x: -500, opacity: 0 }}
                animate={{ x: 0, opacity: 1 }}
                transition={{
                    duration: 1,
                    repeatDelay: 10,
                    repeat: Infinity
                }}
                className='h-full max-container font-semibold flex justify-center items-start flex-col padding-x overflow-x-hidden'>

                <div className='my-6 text-8xl max-lg:text-7xl text-white  max-sm:text-3xl'>
                    <h1>OUR <span className='text-[#f04e3c]'>CLASSES</span> </h1>
                </div>
            </motion.div>
        </section>
    )
}
