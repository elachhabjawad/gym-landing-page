import image from "../assets/gyming.png";

export const Gyming = () => {
    return (
        <section id='about' className=' w-screen min-h-screen'>
            <div className=' max-container flex justify-center items-center gap-24 padding-hero-y padding-x h-full max-xl:gap-7 max-lg:flex-col'>


                <div className=' flex-1'>
                    <p className=' text-[#f04e3c] relative before:absolute before:w-20 before:h-1 before:bg-[#f04e3c] before:top-[50%] before:left-0 pl-24 text-2xl before:translate-y-[-50%]'>
                        REGULAR GYMMING BENEFITS</p>

                    <div className=' my-7 text-5xl leading-[60px] font-semibold  max-xl:text-4xl max-xl:my-4 max-lg:my-7 max-lg:text-5xl max-lg:leading-[60px] max-sm:text-3xl'>
                        <h1>WHY GYMING IS GOOD FOR <br />HEALTH ?</h1>
                    </div>

                    <p className='font text-lg text-slate-500 mb-14  max-xl:mb-8'>
                        World is committed to making participation in the event harass ment free on experience for everyone, regardless of leve of expenc gender by identity and expression oriention disability for personal.
                    </p>



                    <button className='py-4 px-9 text-xl group relative text-white bg-[orangered] rounded-sm' >
                        <div className=' buttonDiv'></div>
                        <span className='buttonSpan'>Join Classes</span>
                    </button>

                </div>


                <div className=' flex-1 w-full'>

                    <img src={image} alt="aboutImg" className='object-cover object-center max-lg:w-full' />

                </div>

            </div>
        </section>
    )
}
